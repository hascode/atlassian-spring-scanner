package com.atlassian.plugin.spring.scanner.maven;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Exclusion;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.apache.maven.shared.dependency.graph.filter.DependencyNodeFilter;
import org.apache.maven.shared.dependency.graph.traversal.CollectingDependencyNodeVisitor;
import org.apache.maven.shared.dependency.graph.traversal.DependencyNodeVisitor;
import org.apache.maven.shared.dependency.graph.traversal.FilteringDependencyNodeVisitor;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * This finds all transitive dependencies of the built artifact and cross reference them to the named
 * scanned deps and includes any transitive deps of those named scanned dependencies by default UNLESS
 * there is 'exclusions' named on the configuration
 */
class ScannedDependencyArtifactBuilder {

    static List<Artifact> buildScannedArtifacts(final DependencyNode projectDependencyGraph, final List<Dependency> configuredScannedDependencies) {

        final List<Artifact> artifacts = new ArrayList<Artifact>();

        projectDependencyGraph.accept(new DependencyNodeVisitor() {

            @Override
            public boolean visit(DependencyNode node) {
                return true;
            }

            @Override
            public boolean endVisit(DependencyNode node) {

                Artifact artifact = node.getArtifact();

                Optional<Dependency> dependencyOption = artifactInConfiguredList(artifact, configuredScannedDependencies);

                if (dependencyOption.isPresent()) {
                    artifacts.addAll(findIncludedTransitiveDeps(node, dependencyOption.get()));
                }

                return true;
            }
        });
        return artifacts;
    }

    private static Optional<Dependency> artifactInConfiguredList(final Artifact artifact, List<Dependency> configuredScannedDependencies) {

        Predicate<Dependency> predicate = new Predicate<Dependency>() {

            @Override
            public boolean apply(Dependency input) {

                if ((input.getArtifactId() != null) && (! "*".equals(input.getArtifactId()))) {

                    return
                            input.getGroupId().equals(artifact.getGroupId()) &&
                            input.getArtifactId().equals(artifact.getArtifactId());

                } else {

                    return input.getGroupId().equals(artifact.getGroupId());
                }
            }
        };

        return Optional.fromNullable(Iterables.find(configuredScannedDependencies, predicate, null));
    }

    /**
     * This method descends the specified node and adds all transitive dependencies to the list of artifacts UNLESS the
     * configuration says that there are exclusions
     *
     * @param dependencyNode the graph node
     * @param dependency     the configuration dependency from maven config
     *
     * @return a list of included transitive deps
     */
    private static List<Artifact> findIncludedTransitiveDeps(DependencyNode dependencyNode, final Dependency dependency) {

        //
        // we want all the transitive deps to be scanned as long as they are not explicitly excluded
        DependencyNodeFilter includedDeps = new DependencyNodeFilter() {

            @Override
            public boolean accept(DependencyNode node) {
                return !isExcluded(node.getArtifact(), dependency.getExclusions());
            }
        };

        CollectingDependencyNodeVisitor collector = new CollectingDependencyNodeVisitor();
        FilteringDependencyNodeVisitor filterer = new FilteringDependencyNodeVisitor(collector, includedDeps);
        dependencyNode.accept(filterer);

        List<DependencyNode> nodes = collector.getNodes();

        return newArrayList(Iterables.transform(nodes, new Function<DependencyNode, Artifact>() {

            @Override
            public Artifact apply(DependencyNode input) {
                return input.getArtifact();
            }

        }));
    }

    private static boolean isExcluded(Artifact artifact, List<Exclusion> exclusions) {

        for (Exclusion exclusion : exclusions) {
            if (exclusion.getGroupId().equals(artifact.getGroupId()) && exclusion.getArtifactId().equals(artifact.getArtifactId())) {
                return true;
            }
        }

        return false;
    }
}

