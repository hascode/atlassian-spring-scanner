package com.atlassian.plugin.spring.scanner.external.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is a component that is transitive
 */
@Component
public class TransitiveJarComponentComposite {
    @Autowired
    TransitiveJarComponent1 externalJarComponent1;

    @Autowired
    TransitiveJarComponent2 externalJarComponent2;
}
