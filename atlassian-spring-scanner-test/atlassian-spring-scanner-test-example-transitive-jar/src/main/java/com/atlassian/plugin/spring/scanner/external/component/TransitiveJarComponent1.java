package com.atlassian.plugin.spring.scanner.external.component;

import org.springframework.stereotype.Component;

/**
 * This is a component that is transitive
 */
@Component
public class TransitiveJarComponent1 {
}
