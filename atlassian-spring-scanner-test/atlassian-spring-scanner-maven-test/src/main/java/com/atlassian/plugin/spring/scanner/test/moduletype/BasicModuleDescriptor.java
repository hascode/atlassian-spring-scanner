package com.atlassian.plugin.spring.scanner.test.moduletype;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class BasicModuleDescriptor extends AbstractModuleDescriptor<String> {
    public BasicModuleDescriptor(@ComponentImport final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public String getModule() {
        return "Basic module is working";
    }
}
