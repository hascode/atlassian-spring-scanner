package com.atlassian.plugin.spring.scanner.test.exported;

public interface ExposedAsAServiceComponentInterfaceTwo {
    void doOtherStuff();
}
