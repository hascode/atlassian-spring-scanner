package com.atlassian.plugin.spring.scanner.test.product.bitbucket;

import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.plugin.spring.scanner.annotation.component.BitbucketComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BitbucketImport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A component that's only instantiated when running in Bitbucket
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@BitbucketComponent
public class BitbucketOnlyComponent {

    private final RepositoryService bitbucketRepositoryService;

    @Autowired
    public BitbucketOnlyComponent(@BitbucketImport RepositoryService bitbucketRepositoryService) {
        this.bitbucketRepositoryService = bitbucketRepositoryService;
    }
}
