package com.atlassian.plugin.spring.scanner.test.unused;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("specific-override")
@Component
public class UnusedSpecificOverride {
}
