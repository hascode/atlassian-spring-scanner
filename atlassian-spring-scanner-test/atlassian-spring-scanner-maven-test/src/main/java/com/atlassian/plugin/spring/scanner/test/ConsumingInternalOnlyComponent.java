package com.atlassian.plugin.spring.scanner.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConsumingInternalOnlyComponent {

    private final InternalComponent internalComponent;
    private final InternalComponentTwo internalComponentTwo;

    @Autowired
    public ConsumingInternalOnlyComponent(final InternalComponent internalComponent, final InternalComponentTwo internalComponentTwo) {
        this.internalComponent = internalComponent;
        this.internalComponentTwo = internalComponentTwo;
    }
}
